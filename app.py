import json
import hashlib
from itertools import product

result = []
alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

for repeat in range(4):
    for tuple_string in product(alphabet, repeat=repeat):
        string = ''

        for i in tuple_string:
            string += i

        encrypted_iteration = hashlib.sha256(string.encode()).hexdigest()

        result.append({string: encrypted_iteration})

with open('result.json', 'w+') as file_out:
    json.dump(result, file_out)
